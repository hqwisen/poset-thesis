% !TEX root = ../thesis.tex

\chapter{Introduction: summary and contributions}

\setcounter{page}{1}

\section{Partially ordered sets and antichains}

\paragraph{}

Data structures play an important role in algorithms complexity.
With the objective to improve standard algorithms in automata theory,
researchers
have implemented new algorithms to solve
important problems therein.
One computer science
field that benefits from
those new implementation is formal verification of computer systems.
Verifiying a system consists in checking for a given model, if
it respect some formal specifications.
The major issue regarding problems
such as model checking is the state-space explosion problem.
Verification techniques are based on representing
models and specifications using automata, and the combination
can lead to an exponential blow-up. To face the state-space explosion
problem, methods that allow to represent larger system using
smaller models were developed. A known method is called
symbolic model checking and sometimes uses a data structure called
Binary Decision Diagram, used to represent boolean functions
\cite{principles-model-checking}.


\paragraph{}

With the same objective to improve algorithms for model checking,
L. Doyen and J-F. Raskin proposed
in \cite{antichain-model-checking} improved algorithms
for the automata-based approach to model checking.
In this improvement, they use another type of data structure
called antichains.
Also, another well known problem in verification is the synthesis problem.
It is, for
some model of a controller and an environment,
to check if there exists a winning strategy for the controller.
In order to develop more efficient algorithms to resolve such
problems Filiot et al. proposed in \cite{antichain-ltl-realizability} a new approach
using antichains.

\paragraph{}


Those new \textit{antichain} approaches
are possible due to a partial order that exists on the state
space of the subset constructions.
A partial order is a binary relations,
that is comparison between two elements,
of a specific set.
In the case of verification-related
problem, the partial order is defined
between set of states of an automaton.

\paragraph{}

Since it has been
proven that subsets of the
state space are closed for some partial order
\cite{antichain-model-checking, antichain-universality},
antichains are well suited to resolve such problems.
Indeed, antichains are a set of incomparable elements, and
allow to represent partially ordered set in a more compact way.
This is possible through different operations on the antichains.
When the set of a partial order is closed, the antichains
can represent a bigger set by only keeping
either maximal or minimal elements,
and other elements can be retrieved using a lower or upper closure,
that is all the elements that are either smaller or bigger
compared to the elements of the antichains
(we define formally
those notions in section \ref{data_structures}).



\section{\texttt{poset}, a new library}

\paragraph{Existing work}

There are two interesting implementations that were found.
The first one is \texttt{AaPAL} (
Antichain and Pseudo-Antichain Library), a generic
library that was implemented in the frame of
Aaron Bohy's PhD thesis \cite{bohy-phd}
to provide an antichain library. It is implemented in \texttt{C}.
The other implementation of antichains
have been done
by De Causmaecker and De Wannemacker in \cite{antichain-in-finite-universe}. The algorithms
to find the ninth Dedekind number uses antichains and they needed to
implement a representation of antichains. Their implementation is using
\texttt{Java}.
To improve efficiency and performances, Hoedt in \cite{ninth-dedekind-java} has extended
\cite{antichain-in-finite-universe} antichains implementation by using bit sequence
instead of tree reprensentation.

One would like to use a solid and maintained automata library and use antichains with it. Because the existing works around antichains lack of nice and simple integration with known automata library, we focus on providing a rather easy to use antichains library and integrate it in \texttt{Owl}, an $\omega$-automata library.

\subsection*{Why implementing a new library}

\paragraph{Owl}

Using antichains to implement automata-based algorithms became really important, and a more
developer-friendly approach is lacking. \texttt{Owl} is a \texttt{Java}
library providing a collection of tools to interact
with $\omega$-words, $\omega$-automata and linear temporal logic. Since most of the ($\omega$-)automata based algorithms
can benificiate from antichains, including functions to interact with such structures within \texttt{Owl} will enable
researchers and developers to use antichains in a well established framework for automata theory.

\paragraph{}

To integrate poset and antichains in \texttt{Owl}, one need to implement those data structures and the
different operations that can be applied to those. Therefore one of the objective of this work is to provide a
framework, a well defined structure to implement new data structures and operations, and default implementation
to interact with the most common use cases. Because \texttt{Owl} is implemented in \texttt{Java} the new library providing the framework functionalities, named \texttt{poset}, will also be implemented using the \texttt{Java} language.

\paragraph{How it works}

In the following chapters we will provide a step by step guide on how to use and implement automata algorithms that require antichain data structures. \texttt{Owl} is highly modular and provides a nice command line interface. After the algorithm is implemented, the modules can be chained as shown in the snippet below.

\begin{lstlisting}[language=bash, numbers=none]
owl -I myautomaton.hoa hoa --- complete --- universality-check --- string
\end{lstlisting}

Here we provide a file containing the description of our automaton (in HOA format introduced in section \ref{sec:owl:hoa}) and chain two operations to complete the automaton first and to compute the universality check. The \texttt{universality-check} module is the antichain based algorithm using the implementation introduced in \cite{antichain-universality} and described in section \ref{sec:owl:universality}. The snippet below gives the main function implementing the algorithm using antichains and orders from the \texttt{poset} library.

\begin{lstlisting}[numbers=none]
Set<S> acceptingStates = getAcceptingStates(automaton);
var start = Set.of(automaton.initialStates());
// We create an antichain for the subsets, using the include order
Antichain<Set<S>> targets = new AntichainList<>(Include::order);
targets.add(getNonAcceptingStates(automaton));
Set<Set<S>> frontier = new HashSet<>(targets);
SubsetEq<S> order = new SubsetEq<>();
// And we compute the algorithm until the condition is met!
while (!frontier.isEmpty() && !order.test(start, frontier)) {
  frontier = computeFrontier(automaton, frontier, targets);
  targets.addAll(frontier);
}
return !order.test(start, frontier);
\end{lstlisting}

\section{Objectives \& contributions}

Our main objective is to implement a new library in \texttt{Java} to interact with antichains and use them within \texttt{Owl} framework to implement automata algorithms. We therefore first implement interfaces and classes for poset, antichains and orders related operations and structures. We will provide some default implementations using sets and vectors universe for the antichains. The second objective is to implement a module to integrate and execute in the \texttt{Owl} framework. Our main contribution is mainly the \texttt{poset} library and the universality check use case implementation, with the desired goal to provide a first step to ease the development and integration of antichain algorithms in automata theory.

\section{Plan of the thesis}

In this paper we try to provide a thorough user guide to interact with the \texttt{poset} library and the integration of algorithms in \texttt{Owl}. There are four main chapters, from 2 to 5, in this document. Chapter 2 lays down the theoretical context required to follow the guide. Chapter 3 introduce the $poset$ library interfaces and default implementation. Some examples are given using the default implementations. Chapter 4 introduces the framework \texttt{Owl} including the different concepts required to understand how it can be used and how a new module can be integrated. It describes the universality check use case implementation and how it can be registered in \texttt{Owl} as a new module to be integrated in the pipelines. Chapter 4 also provides an experimental evaluation of this new implementation and a comparison with another automata platform that implements the state-of-the-art subset construction. In Chapter 5, we give the full implementation of the $poset$ library and analyse it. In the last Chapter 6, we conclude by summarising our work and discuss important points which stems from it.
