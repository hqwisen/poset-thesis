% !TEX root = ../thesis.tex

\chapter{Implementation of \texttt{poset}}

\label{chap:impl}

In this chapter, we describe the implementation of the \texttt{poset} framework.
We discuss the implementation of the methods introduced in chapter \ref{chap:framework}.
Mainly, we give and discuss the default implementation for the different interfaces of the framework.
First we describe orders to interact with sets and vectors. Then we give an implementation of the greatest lower bound.
Finally, we give an implementation of antichains and closed sets using a linked list to store
the (incomparable) elements.

\section{Concepts}

To implement a modular and efficient library one as to think about who and how it will be used. Most
of mathematical concepts such as functions and structures have been studied and theorized by skilled
scientists. For a library to thrive, an important objective should be put toward not reinventing the wheel.
Therefore, in this work an important effort was put into using the concepts and APIs available in the tools that are used.

Mainly, because the library is implemented in \texttt{Java 11} most of the implementation that we provide rely on this version features.

\paragraph{Functional Interfaces}

A functional interface is a class that contains only one abstract method. In \texttt{Java 8+}, a new syntax for
lambda expressions has been introduced which enables developer to create function without instanciating any class.

\paragraph{Built-in data structures}

\texttt{Java} provides many data structures. In our context we are mainly interested about sets and vectors, which
are structures provided as built-ins by \texttt{Java} through the \texttt{Set} and \texttt{List} collections.


\section{Default implementation}

\subsection{Order}

\paragraph{Interface}

As defined in section \ref{sec:prelim:binary-relations}, an order $\preceq$ is a binary relation between
elements from a same universe. An order is implemented  as a function that compares two elements.
It takes two parameters of the same type and return a boolean value, that is true if the first element
is smaller that the second, false otherwise. If two the elements are not comparable, the function must throw
an \texttt{Incomparable} exception.

Therefore, an order function as the following definition:

\begin{lstlisting}[numbers=none]
@FunctionalInterface
public interface Order<T> extends BiPredicate<T, T> {
  boolean test(T t1, T t2);
}
\end{lstlisting}

With an additional default method \texttt{compare()}, which throws an exception if the element are incomparable:

\begin{lstlisting}[numbers=none]
default boolean compare(T a, T b) throws Incomparable {
  if (test(a, b)) {
    return true;
  }
  if (!test(b, a)) {
    return false;
  }
  throw new Incomparable();
}
\end{lstlisting}

\paragraph{Implementation of $\subseteq$ and $\sqsubseteq$}

The implementation of the inclusion order $\subseteq$ can be implemented using the built-in \texttt{Set} structure
and by using the \texttt{Set.containsAll()} method:

\begin{lstlisting}[numbers=none]
public class Include<T> implements Order<Set<T>> {
  @Override
  public boolean test(Set<T> a, Set<T> b) {
    return order(a, b);
  }
}
\end{lstlisting}

For the $\sqsubseteq$ order the algorithm to implement the function is given below.

\begin{lstlisting}[numbers=none]
public class SubsetEq<T> implements Order<Set<Set<T>>> {
  public static <T> boolean test(Set<Set<T>> a, Set<Set<T>> b) {
    boolean containedInB;
    for (Set<T> subsetA : a) {
      containedInB = false;
      for (Set<T> subsetB : b) {
        if (subsetB.containsAll(subsetA)) {
          containedInB = true;
          break;
        }
      }
      if (!containedInB) {
        // If no subset of b contains the subset of a
        return false;
      }
    }
    return true;
  }
}
\end{lstlisting}


\paragraph{Reverse orders}

By default, the library is implemented to compute the operation using lower orders.
One must therefore implement orders with this in mind. To reverse an order, the \texttt{BiPredicate} built-in
interface provides an default method \texttt{negate()} that can be used to inverse the \texttt{test()} method. See Example \ref{negate-example}.

\paragraph{Complexity analysis}

Let $\alpha_1$ and $\alpha_2$ antichains with respectively $n$ and $m$ incomparable elements with $n < m$.
The computation of this operation depends on the computation of the order $\preceq$. Let $a$ and $b$ two sets of
respectively $k$ and $l$ elements with $k < l$. Comparing the sets $a$ amd $b$ is made in $O(k)$. Therefore,
checking the inclusion of the antichains $\alpha_1$ and $\alpha_2$ can be made in $O(kn)$ time.

\subsection{Bound functions}

Bounds, defined in section \ref{sec:prelim:bounds}, are functions that return a \textit{bound} between a set
of elements. The bound is deduced by comparing the elements using an order. Therefore a bound function takes three parameters: two objects and an order and return the bound element. The \texttt{compute()} method of the \texttt{Bound} interface implements this function and has the following signature:

\begin{lstlisting}[numbers=none,language=java]
  T compute(T a, T b, Order<T> order);
\end{lstlisting}

The implementation of the greatest lower bound is provided with library and given below.

\begin{lstlisting}[numbers=none]
public class GreatestLowerBound implements Bound<List<Integer>> {
  @Override
  public List<Integer> compute(List<Integer> a, List<Integer> b,
      Order<List<Integer>> order) {
    try {
      if (order.compare(a, b)) {
        return a;
      } else {
        return b;
      }
    } catch (Incomparable e0) {
      // a and b are the same size. If different, compare will fail.
      Integer[] elements = new Integer[a.size()];
      List<Integer> aV1d, bV1d; // Vector of 1 dimension
      for (int i = 0; i < a.size(); i++) {
        aV1d = List.of(a.get(i));
        bV1d = List.of(b.get(i));
        try {
          elements[i] = order.compare(aV1d, bV1d) ? a.get(i) : b.get(i);
        } catch (Incomparable e1) {
          // should never happened with comparison of 1d vector.
          throw new ComputationError("Comparison vector of 1 dimension failed.");
        }
      }
      return Arrays.asList(elements);
    }
  }
}
\end{lstlisting}

% FIXME should closure be included ?

% \subsection{Closure functions}
%
% \todoin{Work in progress on this implementation}
%
%
% The closure operation computes a listing of the elements symbolically represented by the
% antichain. To this end, the framework implement the following algorithms, given the function
% closure:

\section{Data structures}

\label{sec:implementation:data-structures}

The purpose of the framework is to enable the usage of partially ordered sets and antichains
by inherently using efficient data structures to represent them. In this
section we give the different algorithms implemented in the framework and give a default implementation fo the \texttt{Antichain} and \texttt{ClosedSet} interfaces introduced in Chapter \ref{chap:framework}. The library comes with one default implementation storing the incomparable elements of the antichain in a \texttt{LinkedList}
.
\subsection{Antichains}

The definition of the \texttt{Antichain} interface has been given in
\ref{sec:framework:antichain}. Here we give the implementation of the methods.
The class that implements the \texttt{Antichain} inteface is named \texttt{AntichainList} and uses \texttt{LinkedList} to store the incomparable elements.

\paragraph{Linked list} The data structure used to store the antichain for the default implementation is the \texttt{LinkedList} built-in class. It has been selected in favor of \texttt{ArrayList} because the complextiy for the \texttt{add()} method is equivalent for both implementation where the \texttt{remove()} operation is more efficient with \texttt{LinkedList} on average. % FIXME Where does this explanation comes from ?

\paragraph{Attributes}
The \texttt{AnticahinList} class has two attributes, \texttt{AntichainList.order} which contains an instance of an \texttt{Order<E>} function and \texttt{elements} of the \texttt{LinkedList<E>} which contains the incomparable elements of the antichain.

\subsubsection*{Methods}

\paragraph{Addition}

The add method is implemented as follow: if any elements in the antichain are smaller that the new element, there are removed from the antichain and the new element is added. If the element is smaller than any elements in the antichain, the loop stops and the new element is not added. The \texttt{Java} function implementing this algorithm is given below.

\begin{lstlisting}[numbers=none]
private boolean symbolicAdd(E element) {
  Iterator<E> i = iterator();
  while (i.hasNext()) {
    E a = i.next();
    switch (order.icompare(a, element)) {
      case Order.SMALLER:
        i.remove();
        break;
      case Order.LARGER:
        return false;
      default:
        break;
    }
  }
  elements.add(element);
  return true;
}
\end{lstlisting}

Adding an element to an antichain performs in $O(n)$ in the worst case scenario.

\paragraph{Union} To compute the union the union of two closed sets all the elements of the closed set given by the closure operation must be available in the resulting closed set. To implement this, we used the trivial implementation that creates a new closed sets where the symbolic of the elements is the upper closure of the union of both antichains representing the two closed sets. The algorithm is given below. The union of two antichains can be performed in $O(n*m)$ where $n$ and $m$ are the size of the two antichains.

\begin{lstlisting}[numbers=none]
@Override
public Antichain<E> union(Antichain<E> other) {
  Antichain<E> result = new AntichainList<>(this.order);
  result.addAll(this.elements());
  result.addAll(other.elements());
  return result;
}
\end{lstlisting}

\paragraph{Intersection}

The implementation of the intersection as introduced in section \ref{sec:prelim:operations} can be implemented using the \texttt{bound} function. The implementation is given below.

\begin{lstlisting}[numbers=none]
public Antichain<E> computeBound(Antichain<E> other) {
  Bound<E> boundOp = this.bound;
  if (boundOp == null) {
    throw new ComputationError("Bound is not specified. " +
        "Cannot compute the bound of the two closed sets.");
  }
  Antichain<E> result = new AntichainList<>(this.order, this.bound);
  for (E a : this.elements()) {
    for (E b : other.elements()) {
      result.add(boundOp.compute(a, b, this.order));
    }
  }
  return result;
}

public Antichain<E> intersection(Antichain<E> other) {
  return computeBound(other);
}
\end{lstlisting}


All the other methods implementation use their counter-parts implementation of the \texttt{LinkedList} class.


\subsection{Closed sets}

Partially ordered sets can be either closed or open. Closed set can
be symbolically represented using the data structure of antichains. Therefore a closed set
will be in practice represented by only keeping the elements composing the antichain that
provide the symbolic representation.

\paragraph{}

In the \texttt{poset} library, a \texttt{ClosedSet} is an interface.
The implementations of the \texttt{ClosedSet} interface will mainly differ in the structure used to store the antichain. The default implementation provided in \texttt{poset} is \texttt{ListCLosedSet} that represents
internally the incomparable elements of the antichain in a linked list.

\paragraph{Attributes} The \texttt{ListClosedSet} uses an \texttt{AntichainList} implementation to store the antichain symbolically representing the closed set. The other attributes are the functions required for the closed set operation: \texttt{order}, \texttt{bound} and \texttt{closure}.

\paragraph{Appartenance} To compute the appartenance of an element in a closed set, the algorithm must check if the element belongs to an elements of the lower closure of the closed set. This can be implemented by checking if the element is smaller than any elements of the antichain. If it is the case, then the elements is an element of the lower closure and belongs to the closed set. The method that implement this algorithm is given below

\begin{lstlisting}[numbers=none]
@Override
public boolean contains(E element) {
  if (element != null) {
    for (E a : this.antichain()) {
      if (element.equals(a) || order.test(element, a)) {
        return true;
      }
    }
  }
  return false;
}
\end{lstlisting}

This operation can be computed $O(n)$ time and $O(1)$ space.
