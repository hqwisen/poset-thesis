% !TEX root = ../thesis.tex

\begin{appendices}

\chapter{How to use the \texttt{poset} library}

\label{appendix:howto}

\paragraph{Install Java}

To run \texttt{Owl} and the \texttt{poset} library \texttt{Java 11} is required. Using Ubuntu 18.04 LTS, java
can be installed with this command:

\begin{lstlisting}[numbers=none,language=bash]
apt install openjdk-11-jdk
\end{lstlisting}



\paragraph{Some commands}

The following section describes some commands than can be ran in the \texttt{poset} repository \cite{poset-src}.

The Gradle and the \texttt{gradlew} script can be used to build, run and test the project:

\begin{lstlisting}[numbers=none,language=bash]
./gradlew run --args="-I data/tv05-dataset0/aut90.hoa hoa --- complete  --- universality-check --- string"
/gradlew assemble
# distributions files will be available in the build/ folder
\end{lstlisting}

With Gradle and Java installed, the script for \texttt{jshell} can be executed as follow:

\begin{lstlisting}[numbers=none,language=bash]
scripts/jshell
\end{lstlisting}

This will start an interactive Java shell and the examples from Chapitre \ref{chap:framework} can be ran.

To generate the Javadoc, one can use Gradle and check the build folder:


\begin{lstlisting}[numbers=none,language=bash]
./gradlew javadoc
\end{lstlisting}



\chapter{\texttt{Owl} Universality Check module}

\label{appendix:owl}

\begin{lstlisting}
package poset.algorithms;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import owl.automaton.Automaton;
import owl.automaton.AutomatonUtil;
import owl.automaton.acceptance.OmegaAcceptance;
import owl.factories.ValuationSetFactory;
import owl.run.modules.ImmutableTransformerParser;
import owl.run.modules.InputReaders;
import owl.run.modules.OutputWriters;
import owl.run.modules.OwlModuleParser.TransformerParser;
import owl.run.parser.PartialConfigurationParser;
import owl.run.parser.PartialModuleConfiguration;
import poset.Antichain;
import poset.AntichainList;
import poset.orders.Include;
import poset.orders.SubsetEq;

public final class UniversalityCheck
    implements
    Function<Automaton<Object, OmegaAcceptance>, UniversalityCheck.UniversalityCheckOutput> {

  static final class UniversalityCheckOutput {
    Boolean result;
    Long time;

    public UniversalityCheckOutput(Boolean result, Long time) {
      this.result = result;
      this.time = time;
    }

    @Override
    public String toString() {
      return result.toString() + ' ' + time;
    }
  }

  public static final TransformerParser CLI = ImmutableTransformerParser.builder()
      .key("universality-check")
      .description("Check the universality of an automaton")
      .parser(settings -> {
        var function = new UniversalityCheck();
        return environment -> (input, context) ->
            function.apply(AutomatonUtil.cast(input, Object.class, OmegaAcceptance.class));
      }).build();


  public static <S> Set<S> cpre(Automaton<S, ?> automaton,
      BitSet valuation, Set<S> destinations) {
    Set<S> predecessors = new HashSet<>();
    Set<S> result = new HashSet<>();
    destinations.forEach(destination -> {
      var ps = automaton.predecessors(destination);
      predecessors.addAll(ps);
    });
    predecessors.forEach(predecessor -> {
      var successors = automaton.successors(predecessor, valuation);
      boolean containsAll = destinations.containsAll(successors);
      if (containsAll) {
        result.add(predecessor);
      }
    });
    return result;
  }

  public static <S> Antichain<Set<S>> CPre(Automaton<S, ?> automaton, Set<Set<S>> destinations) {
    ValuationSetFactory factory = automaton.factory();
    Antichain<Set<S>> result = new AntichainList<>(Include::order);
    destinations.forEach(subset -> {
      var valuation = new BitSet();
      for (int bit = 0; bit < factory.alphabetSize(); bit++) {
        valuation.clear();
        valuation.set(bit);
        result.add(cpre(automaton, valuation, subset));
      }
    });
    return result;
  }

  private static <S> Set<Set<S>> computeFrontier(Automaton<S, ?> automaton, Set<Set<S>> frontier,
      Set<Set<S>> F) {
    SubsetEq<S> order = new SubsetEq<>();
    Set<Set<S>> result = new HashSet<>();
    CPre(automaton, frontier).forEach(q -> {
      var check = order.test(Set.of(q), F);
      if (!check) {
        result.add(q);
      }
    });
    return result;
  }

  public static <S, A extends OmegaAcceptance> UniversalityCheckOutput backward(
      Automaton<S, A> automaton) {
    Set<S> acceptingStates = getAcceptingStates(automaton);
    var start = Set.of(automaton.initialStates());
    Set<S> nonAcceptingStates = new HashSet<>(automaton.states());
    nonAcceptingStates.removeAll(acceptingStates);
    Antichain<Set<S>> targets = new AntichainList<>(Include::order);
    targets.add(nonAcceptingStates);
    Set<Set<S>> frontier = new HashSet<>(targets);
    SubsetEq<S> order = new SubsetEq<>();
    long startTime = System.currentTimeMillis();
    while (!frontier.isEmpty() && !order.test(start, frontier)) {
      frontier = computeFrontier(automaton, frontier, targets);
      targets.addAll(frontier);
    }
    long endTime = System.currentTimeMillis();
    long duration = (endTime - startTime); // ms
    var result = !order.test(start, frontier);
    return new UniversalityCheckOutput(result, duration);
  }

  public static <S, A extends OmegaAcceptance> Set<S> getAcceptingStates(
      Automaton<S, A> automaton) {
    Set<S> acceptingStates = new HashSet<>();
    for (var state : automaton.states()) {
      var edges = automaton.edges(state);
      boolean isAccepting = false;
      for (var edge : edges) {
        if (edge.hasAcceptanceSets()) {
          isAccepting = true;
          break;
        }
      }
      if (isAccepting) {
        acceptingStates.add(state);
      }
    }
    return acceptingStates;
  }

  @Override
  public UniversalityCheckOutput apply(Automaton<Object, OmegaAcceptance> automaton) {
    if (!automaton.is(Automaton.Property.COMPLETE)) {
      throw new IllegalArgumentException("Cannot check universality using antichains"
          + " with incomplete automaton");
    }
    return backward(automaton);
  }

  public static void main(String... args) {
    PartialConfigurationParser.run(args, PartialModuleConfiguration.builder("universality-check")
        .reader(InputReaders.HOA)
        .addTransformer(UniversalityCheck.CLI)
        .writer(OutputWriters.TO_STRING)
        .build());
  }
}
\end{lstlisting}

% \chapter{Contents}
%
% \todoin{Include source code of Universality Check}
% \todoin{Explain how the automaton were gen. and benchmarked (python scripts)}
% \todoin{Describe how to build and execute Owl with gradle}
% \todoin{Discuss the scripts: how to gen aut. (python vs SPOT) and how to perf.}
%
% \section{Unused subsections}
%
% \subsection{Data structures}
%
% \notein{This section needs to be updated.
% Also this more blabla that important things (except maybe a poor state-of-the-art) so it can be removed.}
%
% It is necessary to use an existing and efficient library to represent automaton structure.
% Because our implementation is made in \texttt{Java}, in the context of this implementation,
% it is required to find a suitable library for automaton structure to be used alongside
% the \texttt{poset} library.
% it is better suited to use a existing library, at such an implementation is outside
% of the scope this thesis.
%
% For example, universality check problem is to check whether or not a non-deterministic finite automaton
% is universal. To represent an automaton, a first possibility is to use an open source
% library \texttt{java-nfa} \cite{java-nfa} an integrate it with the algorithm implementation.
%
% \todoin{Other libraries: https://github.com/LearnLib/automatalib}
%
% Another possibility is to use the $\omega$-automata implementation that comes with \texttt{Owl}.
%
% Because final objective of this work is to integrate the library with \texttt{Owl}, it has
% been chosen to extend the \texttt{Owl} $\omega$-automata implementation and to extend it
% to support automata with acceptance of finite words.
%
% \section{What other things can be integrated}
%
% \subsection{Automata Emptiness}
%
% \notein{See Maquet PhD thesis}
%
% \subsection{Dedeking number}
%
% \notein{See Hoedt and De Causemacker papers. This is a nice example of antichain algorithm, that
% is not related to automata algorithms}

\chapter{Additional references}


This appendix is a listing of additional resources related to this project.


\section{Antichains}

References about antichain-based algorithms.

\subsection{Automata algorithms}
\paragraph{Universality}
Main paper about universality problem using antichains \cite{antichain-universality}.
In \cite{pushdown-inclusion-problems} M. Van Tang describe how to checking universality and inclusion of Visibly Pushdown Automata. In \cite{antichain-visibly-pushdown} there are some more details about VPA and antichains.

\paragraph{Antichains for synthesis of reactive systems} \cite{bohy-phd}
\paragraph{New algos. for the Emptiness Problem} \cite{maquet-phd}
\paragraph{Antichain (for automata approached) Model Checking} \cite{antichain-model-checking}
\paragraph{Complexity of Universality and related poset algos} \cite{complexity-universality}.
\paragraph{Antichain Algorithms for Finite Automata} \cite{nfa-antichain-algos}

\subsection{Other algorithms}

\paragraph{Dedekind number}
\cite{ninth-dedekind-java, ninth-dedekind-java-src, antichain-in-finite-universe}
\paragraph{Simulation subsimption in Ramsed-baed Buchi} Other techniques for universality testing \cite{ramsey-universality-testing}.


\subsection{Tools}

\paragraph{AaPAL} \cite{aapal}
\paragraph{Acacia} \cite{acacia}


\section{Automata}

Some references about antichains for automata algorithms.

\subsection{Tools}

\paragraph{Owl - $\omega$-automata library} \cite{owl}
\paragraph{java-nfa} Library to represent NFA structure in Java \cite{java-nfa}
\paragraph{libAMoRE++} http://libalf.informatik.rwth-aachen.de/index.php?page=download
\paragraph{VCSN} https://gitlab.lrde.epita.fr/vcsn/vcsn


\end{appendices}
