% !TEX root = ../thesis.tex

\chapter{\texttt{Owl}: Integrating antichain algorithms in an $\omega$-automaton library}

\label{chap:owl}

To implement some particular antichains algorithms, one has to make use of automata structures.
\texttt{Owl} \cite{owl} is a \textit{"Java tool collection library for $\omega$-words, $\omega$-automata
 and linear temporal logic"} implemented at the Technical University of Munich by Jan Kret{\'{\i}}nsk{\'{y}}, Tobias Meggendorfer and Salomon Sickert \cite{owl}.
The purpose of the integration is to make use of the powerful
library build in \texttt{Owl}. Owl is a very modular framework,
and each module can interact with others. The integration therefore consists into combining
\texttt{Owl} and \texttt{poset} functions to implement antichain algorithms and execute them using Owl pipeline
architecture.

\paragraph{}


In this chapter we provide the basic concepts to understand the tools to interact with automata in \texttt{Owl}.
Then we provide a basic tutorial to register a new algorithm in \texttt{Owl} that can be integrated in the pipelines.
We implement and integrate the antichain-based universality check algorithm \cite{antichain-universality} in \texttt{Owl} using the \texttt{poset} library. Finally we provide some experimental results by comparing the performance of our integration with another automata library \texttt{VCSN} \cite{vcsn}. We also compare some metrics with the results found the original paper of the algorithm \cite{antichain-universality}.

\section{Concepts}

\texttt{Owl} is provided either as a command line interface (CLI) or as an API available
in \texttt{Java} and \texttt{C++}.
The API exposes the functions and structures implemented in \texttt{Owl} to third-party libraries.
To use the module-based command line interface, one might called \texttt{Owl} as follow:

\begin{lstlisting}[numbers=none,language=bash]
owl <input reader> --- <multiple modules> --- <output>
\end{lstlisting}

For example, our goal at the end is to be able to run the antichain-based universality check algorithm as follow:

\begin{lstlisting}[numbers=none,language=bash]
owl hoa --- complete --- universality-check --- string
\end{lstlisting}

where \texttt{hoa} is the name of the input reader, \texttt{complete} an algorithm to complete the automaton given as input, \texttt{universality-check} the implementation of the antichain
algorithm and \texttt{string} is the output writer.

\subsection{Modules: Input Readers, Transformers and Output Writers}

A module in \texttt{Owl} is a function taking an input and returning an output. There exists three types of modules:
\textit{Input Reader} a function that takes an input stream (usually a \texttt{String} object) and returns
an object. In the same pipeline, only one Input Reader can be used and it should be the first module.
\textit{Transformers} are modules taking an object given by the Input Reader and provides another object as output.
Multiple Transformers can be chained together and the last one will provide a resulting object.
Finally the last module is the \textit{Output Writer} which is a function that takes the resulting object as a parameter
and write in the output stream the resulting object in the specified format. Figure \ref{fig:owl:modules} shows
a diagram illustrating the pipe-style implemented in \texttt{Owl} to chain modules together.

\begin{figure}[H]
    \begin{center}
      \includegraphics[scale=0.4]{images/owl-pipelines}
    \end{center}
    \caption{Logic of the \texttt{Owl} pipelines. Each module takes an input object and return an output object.
    The first module is the Input Reader following by multiple Transformers modules ending with the Output Writer
    that takes the object returned by the last Transformers and returns an output stream.}
    \label{fig:owl:modules}
\end{figure}


\paragraph{}

For more details about the modules,
the reader is invited to read the original paper \cite{owl} introducing \texttt{Owl} and to
browse the open source repository at the following url:
\href{https://gitlab.lrz.de/i7/owl}{https://gitlab.lrz.de/i7/owl} \cite{owl-src}.

\subsection{Automaton interface and valuations}

\paragraph{Automaton interface}

\texttt{Owl} is a $\omega$-automata library, that means that an automaton is a 5-tuple
$\langle Q, \Sigma, \delta, q_0, F \rangle$ where $F \subseteq Q^\omega$ is the acceptance condition.
Interactions with automata objects is made through the \texttt{Automaton} interface from the \texttt{Owl} library
and has the following definition:

\begin{lstlisting}[numbers=none,language=Java]
public interface Automaton<S, A extends OmegaAcceptance>
\end{lstlisting}

where \texttt{S} is the type of the states $q \in Q$ and $A$ the type of the acceptance condition. The interface
provides different methods to retreive successors or predecessors states and edges. The complete API is available
on \texttt{Owl} website \cite{owl}.

\paragraph{Valuations}

A valuation is an assignment of truth values. In the context of an automaton, a valuation can be used to compute
the transition function to navigate from a state to another. A valuation will be an assignement of truth values
where the logical components are the letters of the alphabet of the automaton that can be used to evaluate
if the automaton can transitioned from one state to another. A valuation can be represented using
a bit set structure. A bit set is a vector of bits where each component of the bit set is a boolean value.
For example a bit set $\{0, 5\}$ indicates the the values at index $0$ and $5$ are set to true.


\begin{example}
    Let $A = \langle Q_A, \Sigma_A, \delta_A, q_{0A}, F_A \rangle$ be the automaton defined
    in Figure \ref{fig:owl:eg-valuation}. We have $Q_A = \{l_0, l_1, l_2, l_3 \}$ and $\Sigma = \{0, 1\}$.
    Retrieving the successors of $l_0$ using the valuation $\{1\}$ should return $\{l_0, l_1\}$.

    \label{eg:owl:valuation}
    \begin{figure}[H]
        \center
        \begin{tikzpicture}[shorten >=1pt,node distance=2cm,on grid,auto]
           \node[state,accepting,initial] (l0)   {$\ell_0$};
           \node[state,accepting] (l1) [right=of l0] {$\ell_1$};
           \node[state,accepting] (l2) [right=of l1] {$\ell_2$};
           \node[state] (l3) [right=of l2] {$\ell_3$};
           \path[->]
            (l0)
            edge  node {1} (l1)
            edge [loop above] node {0,1} ()
            (l1)
            edge node {0,1} (l2)
            (l2)
            edge node {0,1} (l3)
            (l3)
            edge [loop above] node {0,1} ()
            ;
        \end{tikzpicture}
        \caption{Non-deterministic finite automaton from the
        family of automata $A_k$,
        from \cite{antichain-universality}, with $k=3$.}
        \label{fig:owl:eg-valuation}
    \end{figure}
\end{example}

Retrieving successors using \texttt{Owl} API from this example can be implemented as follow:

\begin{lstlisting}[numbers=none,language=Java]
Automaton<?, BuchiAcceptance> automaton = AutomatonUtil
      .cast(rawObject, Object.class, BuchiAcceptance.class);
// We set the valuation to {1}
var valuation = new BitSet();
valuation.set(1);
// Retrieve the initial state l0
var l0 = automaton.onlyInitialState();
// Retrieve the reachable states with the valuation
var successors = automaton.successors(l0, valuation);
// successors ==> {l0, l1}
\end{lstlisting}

In the next sections, we will see how to define an automaton and parse it within the library.

\subsection{Hanoi Omega Automata format (HOA)}

\label{sec:owl:hoa}

For parsing automata, \texttt{Owl} relies on the Hanoi Omega-Automata format (HOA) \cite{hoa} for their
representation. It uses the \texttt{jhoafparser} \cite{jhoafparser}
library to parse and write automaton in the HOA format.
HOA \cite{hoa} is a format defined to represent $\omega$-automata. It supports transition and
state-based acceptance, non-deterministic automaton and many different features.

\paragraph{}

Because NFA and $\omega$-automaton can have the same structural definition but with a different semantic (i.e.
the accepting condition is different, see section \ref{sec:prelim:automata}),
we use the the HOA format to represent both type of automata
\footnote{We agree that using HOA format is not design to represent NFA and
a better approach could have been implemented.
We further discuss this in Chapter \ref{chap:conclusion}.}.

More information about HOA format can be found in the proceeding \cite{hoa} or on their website \cite{hoa-website}.

\subsubsection{Format}

An automaton in HOA is represented with two parts: an header and a body. It has this syntax:

\begin{lstlisting}[numbers=none]
automaton ::= header "--BODY--" body "--END--"
\end{lstlisting}

\paragraph{Header}

Header syntax is described with the following grammar:

\begin{lstlisting}[numbers=none]
header ::= format-version header-item*
format-version ::= "HOA:" IDENTIFIER
header-item ::= "States:" INT
             | "Start:" state-conj
             | "AP:" INT STRING*
             | "Alias:" ANAME label-expr
             | "Acceptance:" INT acceptance-cond
             | "acc-name:" IDENTIFIER (BOOLEAN|INT|IDENTIFIER)*
             | "tool:" STRING STRING?
             | "name:" STRING
             | "properties:" IDENTIFIER*
             | HEADERNAME (BOOLEAN|INT|STRING|IDENTIFIER)*
\end{lstlisting}

The relevant headers in our context are:


\begin{itemize}
  \item \textbf{AP}: Number of atomic propositions. For an NFA, we have one atomic proposition per letter.
  \item \textbf{Start}: Header specifiying the initial states.
  \item \textbf{Acceptance:} Number of acceptance sets and the acceptance condition. To represent a NFA we define
  only one acceptance, and all the accepting states will be part of the single accepting set. The accepting condition
  is semantically irrelevant for a NFA and will be ignored (programmatically at execution).
\end{itemize}

The other headers are optional.

\paragraph{Body}

Body syntax is described with the following grammar:


\begin{lstlisting}[numbers=none]
body             ::= (state-name edge*)*
// the optional dstring can be used to name the state for
// cosmetic or debugging purposes, as in ltl2dstar's format
state-name       ::= "State:" label? INT STRING? acc-sig?
acc-sig          ::= "{" INT* "}"
edge             ::= label? state-conj acc-sig?
label            ::= "[" label-expr "]"
\end{lstlisting}

As the grammar shows, the body is a sequence of states definition including the name of the state and its transitions.
Figure \ref{fig:owl:hoa-example} shows an example of a NFA with its states,
transitions and accepting states using the HOA format.

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
        \centering
        \begin{lstlisting}[numbers=none, frame=none]
        HOA: v1
        States: 4
        Start: 0
        Acceptance: 1 Inf(0)
        AP: 2 "0" "1"
        --BODY--
        State: 0 {0}
        [0 | 1] 0
        [1] 1
        State: 1 {0}
        [0 | 1] 2
        State: 2 {0}
        [0 | 1] 3
        State: 3
        [0 | 1] 3
        --END--
      \end{lstlisting}
      \caption{}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
        \centering
        \begin{tikzpicture}[shorten >=1pt,node distance=2cm,on grid,auto]
           \node[state,accepting,initial] (l0)   {$0$};
           \node[state,accepting] (l1) [right=of l0] {$1$};
           \node[state,accepting] (l2) [below =of l1] {$2$};
           \node[state] (l3) [left=of l2] {$3$};
           \path[->]
            (l0)
            edge  node {1} (l1)
            edge [loop above] node {0,1} ()
            (l1)
            edge node {0,1} (l2)
            (l2)
            edge node {0,1} (l3)
            (l3)
            edge [loop left] node {0,1} ()
            ;
        \end{tikzpicture}
      \caption{}
      \label{fig:owl:hoa-example:automaton}
  \end{subfigure}
  \caption{On the left side a HOA header and body that represent the automaton on the right
  with one accepting set, 4 states and
  label expression of the transitions between states. Note that the accepting states are associated with
  the \texttt{acc-sig} (acceptance signature)
  \texttt{\{0\}}, which indicates that they belong to the single accepting set.}
  \label{fig:owl:hoa-example}
\end{figure}

\paragraph{HOA in \texttt{Owl}}

The HOA Input Reader, implemented in the class \texttt{HOAReader} in the library,
uses the \texttt{jhoafparser} to parse the HOA format stream provided as input and create an \texttt{Automaton} object.
Even though the HOA format supports state and transition based acceptance
\footnote{Actually state-based acceptance is syntactic sugar for the membership of all the outgoing transitions to this set \cite{hoa}.},
\texttt{Owl} automatically uses transition based acceptance and the \texttt{Automaton} object will have only
the edges associated to accepting sets. Note that if the acceptance sets are defined in the \texttt{state-name}
(as we do in Figure \ref{fig:owl:hoa-example}), \texttt{Owl} will still interpret this acceptance signature
and will associate the acceptance set to all the transitions reaching the state. This is relevant for the algorithms that use NFA as one will have to go through the edges to fetch the accepting states.

\section{Create and register modules}

\label{sec:owl:create-module}

\paragraph{Implement a module function}

Let's create a module to integrate in \texttt{Owl} pipelines. The goal will be to implement a module that takes
an automaton as input and output the initial state. Therefore we can design our module as a function that
takes an \texttt{Automaton} object as the first and single parameter and return a \texttt{String}. Using
the functional interfaces of \texttt{Java}, our module \texttt{RetrieveInitialState}
can have the following class definition:

\begin{lstlisting}[numbers=none]
public final class RetrieveInitialState implements Function<Automaton<?, ?>, String>
\end{lstlisting}

The \texttt{apply} method of our function class is rather trivial and can be implemented as follow to get the initial
state:

\begin{lstlisting}[numbers=none]
@Override
public Boolean apply(Automaton<?, ?> automaton) {
  return automaton.onlyInitialState().toString();
}
\end{lstlisting}

\paragraph{Create the command line parser}

To include our function in \texttt{Owl} pipelines, we have to create a parser for our transformer.
This can be done by creating a new transformer parser using the \texttt{TransformerParser} interface:

\begin{lstlisting}[numbers=none]
public static final TransformerParser CLI = ImmutableTransformerParser.builder()
  .key("retrieve-initial-state")
  .description("Get the only initial state of an automaton")
  .parser(settings -> {
    var function = new RetrieveInitialState();
    return environment -> (input, context) ->
      function.apply(AutomatonUtil.cast(input, Object.class, OmegaAcceptance.class));
  }).build();
\end{lstlisting}

\paragraph{Register and use our module}

Finally, the last step is to register our module into \texttt{Owl} registry:

\begin{lstlisting}[numbers=none]
DEFAULT_REGISTRY.register(RetrieveInitialState.CLI);
\end{lstlisting}

Now our module will be available through \texttt{Owl} command line interface and can be chained with other modules.
For example one can execute the tool with the following command to output the initial state of an automaton:

\begin{lstlisting}[numbers=none,language=bash]
owl -I path/to/automaton.hoa \
    hoa --- retrieve-initial-state --- string
\end{lstlisting}


\section{Integration use case: universality check using antichains}

\label{sec:owl:universality}

In this section we go through the implementation and integration of the antichain-based algorithm
to check if the language of an automaton is universal
\footnote{The full implementation of the module is available
on the GitLab repository \cite{poset-src} of the project.}.
We use the backward algorithm defined in \cite{antichain-universality} that we implements in \texttt{Java}.
The partial orders and antichains are implemented using the \texttt{poset} library
and the automaton using the \texttt{Owl} automaton structures.
Figure \ref{fig:owl:universality-check} illustrate the logic of implementation.

\begin{figure}[H]
    \begin{center}
      \includegraphics[scale=0.4]{images/owl-universality-pipelines}
    \end{center}
    \caption{Logic of the universality check algorithm integration in \texttt{Owl} pipelines.
    The algorithm is implemented as a new module (a transformer) and the \texttt{poset} library is used within
    this module. The HOA Reader used to parse the automaton is the one provided within the \texttt{Owl} framework.}
    \label{fig:owl:universality-check}
\end{figure}

\subsection{Implementation}

The universality check consists into defining whether an automaton accepts all words of
its universe. It takes a NFA \index{nfa} as input and output true if the language of the NFA is universal,
false otherwise. We implement the backward antichain algorithm for testing universality
presented in \cite{antichain-universality},
that we provide in Algorithm \ref{algo:apps:backward-universality}.

\begin{algorithm}[H]
\SetAlgoLined
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Input{an NFA $A = \langle Q, \Sigma, \delta, q_0, F_Q \rangle$}
\Output{Whether $L(A) = \Sigma^*$ is true}
$Start \leftarrow \{q_0\}$ \;
$F \leftarrow \{\overline{F_A}\}$ \;
$Frontier \leftarrow F$ \;
 \While{$(Frontier \neq \emptyset) \wedge  (Start \not\sqsubseteq Frontier)$}{
  $Frontier \leftarrow \{q \in CPre^A(Frontier) | q \not\sqsubseteq F\}$\;
  $F \leftarrow F \sqcup Frontier$
 }
 \Return $Start \not\sqsubseteq Frontier$ \;
 \caption{Backward antichain algorithm for universality check of an automaton}
 \label{algo:apps:backward-universality}
\end{algorithm}


\paragraph{Data structures and operations}

The universality check implementation requires the automaton structure and its operations for
successors, predecessors and retrieving the states of an automaton. For the test,
it uses the function $CPre^A(q)$
defined in section \ref{sec:prelim:notations}. The algorithm make use of of the
$\sqsubseteq$ defined in section \ref{sec:prelim:orders}.

\paragraph{Backward algorithm snippet}


The method in \texttt{Java} that implements this backward algorithm can be done as follow:

\begin{lstlisting}[numbers=none]
public static <S, A extends OmegaAcceptance> boolean backward(
  Automaton<S, A> automaton) {
  Set<S> acceptingStates = getAcceptingStates(automaton);
  var start = Set.of(automaton.initialStates());
  Set<S> nonAcceptingStates = new HashSet<>(automaton.states());
  nonAcceptingStates.removeAll(acceptingStates);
  Antichain<Set<S>> targets = new AntichainList<>(Include::order);
  targets.add(nonAcceptingStates);
  Set<Set<S>> frontier = new HashSet<>(targets);
  SubsetEq<S> order = new SubsetEq<>();
  while (!frontier.isEmpty() && !order.compare(start, frontier)) {
    frontier = computeFrontier(automaton, frontier, targets);
    targets.addAll(frontier);
  }
  return !order.compare(start, frontier);
}
\end{lstlisting}

\subsection{Integration}

\texttt{Owl} reads an automaton as input using the HOA format and automatically translates state acceptance
as transition-based acceptance. Therefore to retrieve the accepting states of an automaton one has to get the transitions
belonging to the acceptance set and consider the target set as an accepting set.

We implement the following method to retrieve the set of accepting states from an automaton parsed using
the \texttt{HOAReader}:

% TODO Complexity of this ? What is the impact on the performance

\begin{lstlisting}[numbers=none]
public static <S, A extends OmegaAcceptance> Set<S> getAcceptingStates(
  Automaton<S, A> automaton) {
  Set<S> acceptingStates = new HashSet<>();
  for (var state : automaton.states()) {
    var edges = automaton.edges(state);
    boolean isAccepting = !edges.isEmpty();
    for (var edge : edges) {
      if (!edge.hasAcceptanceSets()) {
        isAccepting = false;
        break;
      }
    }
    if (isAccepting) {
      acceptingStates.add(state);
    }
  }
  return acceptingStates;
}
\end{lstlisting}

\paragraph{Module}

The universality check algorithm is a function that takes an automaton and returns \texttt{true} or \texttt{false}.
Therefore the class definition of the module can be set to:

\begin{lstlisting}[numbers=none]
public final class UniversalityCheck implements Function<Automaton<?, ?>, Boolean>
\end{lstlisting}

And the function call can be implemented as follow:

\begin{lstlisting}[numbers=none]
@Override
public Boolean apply(Automaton<Object, OmegaAcceptance> automaton) {
  if (!automaton.is(Automaton.Property.COMPLETE)) {
    throw new IllegalArgumentException("Cannot check universality using antichains"
        + " with incomplete automaton");
  }
  return backward(automaton);
}
\end{lstlisting}

The module can then be registred using the instructions given in section \ref{sec:owl:create-module}. After registration the module can be used as follow:

\begin{lstlisting}[numbers=none]
owl -I path/to/automaton.hoa hoa --- \
complete --- univerlisality-check --- string
\end{lstlisting}

Note that for the backward antichain algorithm to work, the automaton must be complete \cite{antichain-universality}. The \texttt{module} is provided with the \texttt{Owl} framework and automatically complete the automaton by adding a sink state for all the non-existing transitions.

All the code to implement the class of the \texttt{universalty-check} module is available in Appendix \ref{appendix:owl}.

\section{Experimental evaluation and comparison}

\subsection{Methodology}

\label{sec:owl:exp:methodology}

\paragraph{Randomized model for automata generation}

For our experimental evaluation we will use the same methodoly described in original paper \cite{antichain-universality} to generate the automata. To generate the NFAs a randomized model has been used. This model has been introduced by \cite{tv05} and has been implemented to compare the performance of automata algorithms.

This list provides a description of the randomized model:

\begin{itemize}
  \item Fixed alphabet $\Sigma = \{0, 1\}$
  \item There is only one initial state
  \item A number $r_{\sigma} = \frac{k_{\sigma}}{|Q|}$, the transition density of $\sigma$, where $k_{\sigma}$ is a number of state pairs, chosen uniformly at random, $(s, s') \in Q \times Q$ where the transition $\delta(s, \sigma) = s'$ is added to the automaton. For all the experiments performed in this work (and the original paper) $r = r_0 = r_1$ where $r$ denotes the \textit{transition density}.
  \item The number $f = \frac{m}{|Q|}$ corresponds to the \textit{densitiy of accepting states}. $m$ corresponds to the number of accepting states of the randomly generated automaton and $m \leq |Q|$ therefore $0 \leq f \leq 1$.
\end{itemize}

\paragraph{Main comparison}

To evaluate the performance of the implemented algorithm in \texttt{Owl} we compare the performance of the backward antichain algorithm with \texttt{VCSN} \cite{vcsn} developed by LRDE. It is a platform for weighted automata and rational expressions. It is implemented in \texttt{C++} and provides \texttt{Python} bindings to interact with it. It provides multiple operations that can be applied to automata including the subset construction, the complement operation and the emptiness check that will enable us to check for the universality of an automaton. \texttt{VCSN} has been chosen to compare our implementation because it is a maintained and stable platform that provide multiple features to interact with finite state machines.

For each sample point $(r, f)$ we generate 100 random automata using the model described above with $|Q| = 175$. The measure the wall-clock execution time of the algorithm itself without taking into consideration the input parsing and the construction of the data structures as performed in \cite{antichain-universality} evaluation.

\paragraph{Subset construction with VCSN}

To check the universality of an automaton with \texttt{VCSN} we will use the API of VCSN to determinize and check the emptiness of the complement of this result. With the \texttt{Python} bindings the subset construction algorithm can be implemented as follow:

\begin{lstlisting}[numbers=none, language=python]
def universality_check(a):
    # determinize performs the subset construction
    # is_useless -> automaton accepts no words
    # so if the complement is useless -> original is universal
    return a.determinize().complete().complement().is_useless()

# autstring is a variable containing the automaton in VCSN format
aut = vcsn.automaton(autstring, strip=False)
is_universal = universality_check(aut)
\end{lstlisting}

This is the implementation that will be compared against the universality check module integrated in \texttt{Owl}.

\paragraph{Comparison with the reference paper}

We will also compare our results with the evaluation described in \cite{antichain-universality}. In this article, were they provide the original implementation of the antichain-based universality check algorithm, they compare their implementation, made on top of \texttt{NuSMV} \cite{nusmv} by using BDDs to encode the NFAs and the antichains of state sets, with another implementation using the subset construction. This algorithm written in \texttt{Java} is available in an automata library named \texttt{dk.bricks.automaton}. This implementation compute the universality check by building the deterministic equivalent of the NFA given as parameter and then checks for the emptiness of the complementary automaton. The methodology used is the same as described in \ref{sec:owl:exp:methodology}.

Note that we compare our results with the results of the reference paper directly without executing their algorithms into the same machine.

\subsection{Results}

As mentioned in the methodology section above we will compare the universality check module from \texttt{Owl} with \texttt{VCSN} subset construction algorithm to show that the performance of the antichain algorithm are better than the usual subset construction. Then we will compare our results with the reference implementation.
Our goal is to evaluate whether the implementation integrated in \texttt{Owl} is as performant as the one described in the original paper \cite{antichain-universality}.

All the experiments were run on a Linux workstation on top of Intel Core i5-7300U CPU at 2.60GHz with 16GB of RAM. All the scripts used to perform the evaluation and to generate the results are available in the project repository \cite{poset-src}.

\begin{figure}[H]
  \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[scale=0.5]{images/this-average-antichain}
      \caption{Average execution time for the antichain algorithm in \texttt{Owl}}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[scale=0.27]{images/original-average-antichain}
      \caption{Average execution time for the semi-symbolic antichain algorithm}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[scale=0.5]{images/this-average-subset}
      \caption{Average execution time for the subset algorithm in \texttt{VCSN}}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[scale=0.27]{images/original-average-subset}
      \caption{Average execution time for the subset algorithm in \texttt{dk.brics.automaton}}
  \end{subfigure}
  \caption{Results of the experiments for this work on the left and the reference paper \cite{antichain-universality} on the right (extracted from the article for a side by side comparison). All sample points correspond to the average execution time in milliseconds of 100 automata with $|Q| = 175$.}
\end{figure}


\paragraph{Discussion on the results}

The first results that can be highlighted is that the antichain algorithms always perform better. The curves do match the results found in the reference paper, but our implementation is still nearly 10 times slower. This might be explained by the fact that the original implementation use a specific encoding for the automata that we do not implement at all. We use the default \texttt{Automaton} interface provided by \texttt{Owl} where one need to navigate through the automaton to discover the transitions and the states. Nonetheless, the performance are still sastifying (runs in less that 1 second) enough for a rather simple implementation within \texttt{Owl} and shows that there are no exponential blew up when complex frameworks to implement antichain algorithms are used.
