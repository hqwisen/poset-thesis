% !TEX root = ../thesis.tex

\chapter{Poset \& antichains, automata and language universality}

\section{Data Structures}


\label{data_structures}

\paragraph{}

In this section, we will provide formal definitions of the data
structures that we will implement. We recall the notion of binary relations
and important propreties of such relations.
We then define partially ordered set, totally order set and closed set.
Finally we give a formal definition for antichains.

\paragraph{}

The definitions and examples for this section are based on \cite{bohy-phd}
and \cite{maquet-phd}.


\subsection{Binary relations}

\label{sec:prelim:binary-relations}

\paragraph{}

A binary relation for an arbitrary set $S$ is
a set of pair $R \subseteq S \times S$.
There are five important properties: reflexitivity, transitivity,
symmetry, antisymmetry and total.

\paragraph{}

A relation $R$ on $S$ is said to be:

\begin{itemize}
    \item Reflexive:
    iff $\forall s \in S$ it holds that $(s, s) \in R$
    \item Transitive:
    iff $\forall s_1, s_2, s_3 \in S$,
    if ($s_1, s_2) \in R$ and $(s_2, s_3) \in R$
    then it holds that $(s_1, s_3) \in R$
    \item Symmetric: iff $(s_1, s_2) \in R$ then $(s_2, s_1) \in R$.
    \item Antisymmetric: iff $(s_1, s_2) \in R$
    and $(s_2, s_1) \in R$ then $s_1 = s_2$
    \item Total: iff $\forall s_1, s_2 \in S$ then $(s_1, s_2) \in R$
    or $(s_2, s_1) \in R$

\end{itemize}

\paragraph{Orders}

\label{sec:prelim:orders}

A \textit{partial order} is a binary relation that is \textit{reflexive},
\textit{transitive} and \textit{antisymmetric}. We note a
partial order relation by $R$.
We note $s_1 R  s_2$ to show the belonging of
a binary relation to a partial order, which is equivalent
to $(s_1, s_2) \in \ R$.
A \textit{total order} is a partial order that is \textit{total}.

\begin{example}

For example, the comparison of natural numbers is a partial order.
Let $\leq$ be a binary relation on $\mathbb{N}$
such that $\leq \ \subseteq \mathbb{N}^2$. The binary relation is defined
following the usual semantic of the symbol, i.e. $n_1 \leq n_2$ if and
only if $n_1$ is smaller or equal to $n_2$.
Based on this, $\leq$ is a partial order. It is
reflexive, transitive and antisymmetric.
The binary relation $\leq$ on natural numbers is actually a total
order since all the natural numbers can be compared against each other.

\end{example}

\paragraph{$\sqsubseteq$ partial order}

We define this additional partial order between two antichains $q, q'$ over the set of states $Q$: let $q \sqsubseteq q'$ if and only if $\forall s \in q \cdot \exists s' \in q': s \subseteq s'$.



\subsection{Partially ordered set}

\paragraph{}

An arbitrary set $S$ associated with a partial order $\preceq$
is called a \textit{partially ordered set} or \textit{poset}.
It is denoted by the pair $\langle S, \preceq \rangle$.

\paragraph{Comparable}

Let $s_1, s_2 \in S$ and $\tuple{S, \preceq}$ a poset.
The two elementes $s_1$ and $s_2$ are said to be \textit{comparable} if either
$s_1 \preceq s_2$ or $s_2 \preceq s_1$. If neither of those two comparisons
are correct, then $s_1$ and $s_2$ are said to be \textit{incomparable}.


\paragraph{Bounds}

\label{sec:prelim:bounds}

Let $\tuple{S, \preceq}$ a partially ordered set.
A \textit{lower bound} of $P \subseteq S$ is an element $s \in S$
such that for all $p \in P$, it holds that $s \preceq p$.
The \textit{greatest lower bound} of elements of a set $P \subseteq S$
is an element $s \in S$ defineds as follow:
for all $p \in P$
it holds that $s \preceq p$, and for all $s' \in S$ we have that
if $s' \preceq p$ then $s' \preceq s$.

The greatest lower bound is unique. It means that if two elements
$s_1, s_2 \in S$ are $\preceq$-incomparable and for a subset $P \subseteq S$,
for all $p \in P$, if it holds that $s_1 \preceq p$ and $s_2 \preceq p$
and if all others elements $s' \in S$ with $s'$ a lower-bound
for $P$ such that $s' \preceq s_1$ and $s' \preceq s_2$;
the greatest lower bound is said to be undefined.

For a set of two elements
$P = \{p_1, p_2\}$, we denote by $p_1 \sqcap p_2$ the greatest lower bound.

\paragraph{Lattices} A \textit{lower semilattice} is a poset
$\tuple{S, \preceq}$ where for all pair of elements $s_1, s_2 \in S$,
we have that the greatest lower bound $s_1 \sqcap s_2$ exists.


%\todo{Include definition of least upper bound, if necessary}

\begin{example}
    % the inclusion $\subseteq$ is a partial order on
    % $2^{\mathbb{N}}$,
    % the power set of natural numbers.

    \label{eg:poset2}

%A more interesting example is the set inclusion comparison.
%Let $A$ be automaton defined in Example \ref{universality_eg}.
Let $\tuple{2^{Q_A}, \subseteq}$, a poset with
$Q_A$ a set of 3 elements. Figure \ref{fig:eg_antichain1}
shows a graph with the incomparable elements of such poset.

\begin{figure}

    \center
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick,
                    main node/.style=
                    {rectangle,draw,font=\sffamily\large\bfseries}]

  \node[main node] (012) {$\{q_0, q_1, q_2\}$};
  \node[main node] (02) [below of=012] {$\{q_0, q_2\}$};
  \node[main node] (01) [left of=02] {$\{q_0, q_1\}$};
  \node[main node] (12) [right of=02] {$\{q_1, q_2\}$};
  \node[main node] (1) [below of=02] {$\{q_1\}$};
  \node[main node] (0) [left of=1] {$\{q_0\}$};
  \node[main node] (2) [right of=1] {$\{q_2\}$};
  \node[main node] (EMPTY) [below of=1] {$\emptyset$};

  \path[every node/.style={font=\sffamily\small}]
      (02) edge node [] {} (012)
      (01) edge node [] {} (012)
      (12) edge node [] {} (012)

      (0) edge node [] {} (01)
          edge node [] {} (02)
      (1) edge node [] {} (01)
          edge node [] {} (12)
      (2) edge node [] {} (02)
          edge node [] {} (12)

      (EMPTY) edge node [] {} (0)
              edge node [] {} (1)
              edge node [] {} (2)
    ;
\end{tikzpicture}
    \caption{Example of antichains using
    the poset $\tuple{2^{Q_A}, \subseteq}$,
    with $Q_A$ the set of states of an automaton $A$, which correspond
    to $Q_A = \{q_0, q_1, q_2\}$. Each directed edge of the graph
    correspond to a valid comparison using the set inclusion $\subseteq$.
    For example
    $\{q_0\} \rightarrow \{q_0, q_1\}$ corresponds to
     the inclusion $\{q_0\} \subseteq \{q_0, q_1\}$. Two elements of the graph
     with no connection, means that the elements are incomparable. For
     example, $\alpha = \{\{q_0\}, \{q_1\}, \{q_2\}\}$ is a set
     of incomparable elements and $\alpha$ is called an antichain.}
     \label{fig:eg_antichain1}
\end{figure}

\end{example}

\subsection{Antichains}

\paragraph{Closed sets}

A closed set is a set $L \subseteq S$
of a lower semilattice $\langle S, \preceq \rangle$
where $\forall \ell \in L$ we have that $\forall s \in S$ such that
$s \preceq \ell$, then $s \in L$.
Note that for two closed sets $L_1, L_2 \subseteq S$, we have that
$L_1 \cup L_2$ and $L_1 \cap L_2$ are also closed sets,
but $L_1 \setminus L_2$ does not result necessarily to a closed set.

\paragraph{Maximal/minimal elements} We denote by $\ceil{L}$
the set of maximal elements of a closed set $L$ which
%\todo{Meaning of | vs . vs : in set definition ?}
correspond to $\ceil{L} =
\{ \ell \in L | \forall \ell' \in L : \ell \preceq \ell'
 \Rightarrow \ell = \ell' \}$. Alternatively, to represent the set of minimal
 elements, the noation $\floor{L}$ is used which has the following semantic
$\floor{L} = \{ \ell \in L | \forall \ell' \in L :  \ell' \preceq \ell
 \Rightarrow \ell = \ell' \}$.


\paragraph{Closure} A \textit{lower closure} of a set $L$ on $S$
noted $\darrow L$ is the set of all elements of $S$ that are
\textit{smaller or equal} to an element of $L$ i.e.
$\darrow L = \{ s \in S \ | \ \exists \ell \in L \cdot s \preceq \ell\}$.
Note that for a closed set $L$ we have that $\darrow L = L$.

\paragraph{Antichain}

An \textit{antichain} of a poset $\tuple{S, \preceq}$
is a set $\alpha \subseteq S$ where all element of $\alpha$
are incomparable with respect to the partial order $\preceq$.
Otherwise, if all elements are comparable the set is called a \textit{chain}.
Antichains allow to represent closed set in a more compact way.
For a closed set $L \subseteq S$ we can retrieve all elements of $L$ by using
the antichain $\alpha = \ceil{L}$. With respect
to the definition of the lower closure we have that $\darrow \alpha = L$.

\subsection{Operations on antichains}

\label{sec:prelim:operations}

\paragraph{}

This section list the classical propreties of antichains.
All the examples are based on the poset
defined in Example \ref{eg:poset2}.

\begin{proposition}

\label{antichains_ops}

Let $\alpha_1, \alpha_2 \subseteq S$ two antichains and $s \in S$:

\begin{itemize}
    \item $s \in \darrow \alpha_1$
    iff $\exists a \in \alpha_1$ such that $s \preceq a$

    \begin{example}
        Let $\alpha_1 = \{\{q_0, q_1\}\}$, $\{q_0\}$ belongs to
        the lower closure $\darrow{\alpha_1}$ because,
        $\{q_0\} \subseteq \{q_0, q_1\}$.
    \end{example}

    \item $\darrow \alpha_1 \subseteq \darrow \alpha_2$
    iff $\forall a_1 \in \alpha_1,
    \exists a_2 \in \alpha_2$ such that $a_1 \preceq a_2$
    \begin{example}
    Let $\alpha_1 = \{\{q_0\}, \{q_1\}\}$ and
    $\alpha_2 = \{\{q_0, q_1\}\}$,
    $\darrow{\alpha_1} \subseteq \darrow{\alpha_2}$, since
    all elements of $\alpha_1$ are included
    in the single element of $\alpha_2$.
    \end{example}
    \item $ \darrow \alpha_1 \ \cup \darrow \alpha_2 =
    \darrow \ceil{\alpha_1 \cup \alpha_2}$

    \item $\darrow \alpha_1 \ \cap \darrow \alpha_2 =
    \darrow \ceil{\alpha_1 \sqcap \alpha_2}$ where $\alpha_1 \sqcap \alpha_2$
    is defined as
    $\alpha_1 \sqcap \alpha_2 = \{a_1 \sqcap a_2 | a_1
    \in \alpha_1, a_2 \in \alpha_2\}$, that is the greatest lower bound
    of elements between the two antichains.

\end{itemize}

\end{proposition}

\section{Finite Automata}

\label{sec:prelim:automata}

\subsection{Nondeterministic Finite Automta}

\label{sec:prelim:nfa}


A Nondeterministric Finite Automaton (NFA) is a 5-tuple $\langle Q, \Sigma, \delta, q_0, F \rangle$, where:

\begin{itemize}
  \item $Q$ is a finite set of states
  \item $\Sigma$ is a finite set of input, called the alphabet
  \item $\delta$ is the transition function $\delta: Q x \Sigma \to Q$
  \item $q_0 \in Q$, the initial state
  \item $F \subseteq$ is a set of accepting states

\end{itemize}

\subsection{$\omega$-automata}

\label{prelim:automata:omega}

An $\omega$-automaton is a variant of automata defined in section \ref{sec:prelim:nfa}. It runs on infinte
words, therefore do not stop. The main difference remains on the accepting condition where it is not a finite
set of states but a subset of $Q^\omega$, formally $F \subseteq Q^\omega$.

\subsection{Functions}

\label{sec:prelim:notations}

\paragraph{Predecessors functions}

Through the document we make use of the following function definitions, where $q$ is an antichain on $Q$:

\begin{equation}
\textrm{cpre}^A_\sigma(s) = \{q \in Q | \forall q' \in Q: \delta(q, \sigma) = q' \rightarrow q' \in s \}
\end{equation}


\begin{equation}
\textrm{CPre}^A(q) = \ceil{\{s | \exists s' \in q \cdot \exists \sigma \in \Sigma : s = \textrm{cpre}^A_\sigma(s') \}}
\end{equation}


\section{Language universality using antichains}

An example that highlights the use of antichains in automata theory is
the universality problem.
It is the problem that for a finite automaton, we want to
check if the language of this automaton equivalent to the language
of all the words on the alphabet. The universality problem is a classical
theoritical problem, and many verification-related problems can be
reduced to it.

\paragraph{}

Let $A=\tuple{Q_A, \Sigma, q_0, \delta, F_A}$ be a finite automaton,
we want to check if
the language of $A$ is universal, that is, $L(A) = \Sigma^*$.
The language of $A$ is universal if and only if the language
of the complement of $A$ accepts no words, that is,
$L(A) = \Sigma^*$ if and only if $L(\bar{A}) = \emptyset$.
Therefore, the goal is to find a computation path of the
automaton on a word such that the path start in the initial state,
and the final target is a non-accepting state.

\paragraph{}

Let's first define an interesting proprety of non-deterministic automata
before explaining the use of antichains for the universality problem.
Let $q$ and $f$ be two subsets of states such that $q, f \subseteq Q_A$.
When reading a letter $\sigma$ on the deterministic equivalent
$A_d$, that is $q \xrightarrow{\sigma} f$, it is known that
for all sets of states $q' \subseteq q$, the resulting subset
$f'$, when computed on $A_d$ such that $q' \xrightarrow{\sigma} f'$,
it holds that $f' \subseteq f$.
%\todo{Does this proprety have a specific name ?}

\subsection{Concrete example}

\paragraph{}

Let's take the example from \cite{antichain-universality}
shown in Figure \ref{fig:univnondet}. This example is a
non-deterministic finite automata with 4 states where
only the last state is non-accepting.
The goal is to check
if whether or not this automaton is universal.
One approach to check universality, is to check if the
complementary automaton accepts the empty set.
This is actually the standard algorithm that first builds
the equivalent deterministric automaton, then builds the complement
and finally check the emptiness of this final construction.
The complexity here rely on building the equivalent deterministic
which is exponential in size. The idea of the antichain based
algorithm is instead of determinize the automaton, to
do it implicitly by checking only maximal subsets of states.

\paragraph{}

Figure \ref{fig:univdet} shows the equivalent deterministic finite
automaton of the one
shown in Figure \ref{fig:univnondet}.
In the specific example of \ref{fig:univdet}, instead on checking
each state of the deterministic finite automaton, it is
better to check only the maximal path.
We refer to maximal path, a path where each subset of states
when computing any letter is maximal.


Indeed, the focus
is made on checking if the final state of a word computation is
non-accepting that is $q \xrightarrow{w} f$ we have that
 $f \cap F_A = \emptyset$.
Because of the
proprety of state inclusion, checking only the
maximal set of states is sufficient. Let $k=3$, computing the word $1^k$,
will lead to a maximal path for this automaton.
For any word $w < 1^k$
of size $k$ such that $w = \sigma_1..\sigma_k$ with $\sigma_i \in \Sigma$,
the path of computation will be included in the path of $1^k$,
\textit{i.e.} the subset of states $q_i$ will be included
in $p_i$, that is $q_i \subseteq p_i$,
corresponding respectively to the subsets of states
when reading the $i^{th}$ letter of the word $w$ and $1^k$.
Since the objective is to find the final state as a non-accepting,
if the final state is non-accepting for $1^k$,
that is $p_k \cap F = \emptyset$ then for the word $w$,
since $q_k \subseteq p_k$, it holds that $q_k \cap F = \emptyset$.
This method allow to bypass some computation, \textit{i.e.}
checking the intersection with the accepting states set, by only checking
the maximal subsets of states.


\begin{figure}
    \center
    \begin{tikzpicture}[shorten >=1pt,node distance=2cm,on grid,auto]
       \node[state,accepting,initial] (l0)   {$\ell_0$};
       \node[state,accepting] (l1) [right=of l0] {$\ell_1$};
       \node[state,accepting] (l2) [right=of l1] {$\ell_2$};
       \node[state] (l3) [right=of l2] {$\ell_3$};
       \path[->]
        (l0)
        edge  node {1} (l1)
        edge [loop above] node {0,1} ()
        (l1)
        edge node {0,1} (l2)
        (l2)
        edge node {0,1} (l3)
        (l3)
        edge [loop above] node {0,1} ()
        ;
    \end{tikzpicture}

    \caption{Non-deterministic finite automaton from the
    family of automata $A_k$,
    from \cite{antichain-universality}, with $k=3$.}
    \label{fig:univnondet}


\end{figure}

\begin{figure}
\center
\begin{tikzpicture}[shorten >=1pt,node distance=2cm,on grid,auto]
   \node[state,accepting,initial] (l0)   {$\ell_0$};
   \node[state,accepting] (l0l1) [right=of l0] {$\ell_0,\ell_1$};
   \node[state,accepting] (l0l2) [right=of l0l1] {$\ell_0, \ell_2$};
   \node[state,accepting] (l0l1l2) [below=of l0l2] {$\ell_0,\ell_1,\ell_2$};
   \node[state,accepting] (l0l3) [right=of l0l2] {$\ell_0, \ell_3$};
   \node[state,accepting] (l0l2l3) [below=of l0l3] {$\ell_0,\ell_2,\ell_3$};
   \node[state,accepting] (l0l1l2l3) [below =of l0l2l3]
                                     {$\ell_0,\ell_1,\ell_2,\ell_3$};
   \path[->]
    (l0)
    edge  node {1} (l0l1)
    edge [loop above] node {0} ()
    (l0l1)
    edge node {0} (l0l2)
    edge node {1} (l0l1l2)
    (l0l2)
    edge node {0} (l0l3)
    (l0l1l2)
    edge node {0} (l0l2l3)
    edge node {1} (l0l1l2l3)
    (l0l3)
    edge [loop right] node {0,1} ()
    (l0l2l3)
    edge [loop right] node {0,1} ()
    (l0l1l2l3)
    edge [loop right] node {0,1} ()

    %       edge  node [swap] {1} (q_2)
    % (q_1) edge  node  {1} (q_3)
    %       edge [loop above] node {0} ()
    % (q_2) edge  node [swap] {0} (q_3)
    %       edge [loop below] node {1} ()
    ;
\end{tikzpicture}

\caption{Deterministic automaton equivalent of the non-deterministic
automaton shown in Figure \ref{fig:univnondet}.}
\label{fig:univdet}

\end{figure}


% The only way to compute the complementary of $A$ is to first
% determinise it, which is hard. Let define $A_d$ as the deterministic finite
% automaton (DFA) of $A$ such that $L(A_d) = L(A)$.
% When $A$ is transformed to $A_d$,
% the set of states of the DFA is the powerset of states,
% that is a state of the DFA $A_d$ is represented by a subset of state of $A$.
% Now if $L(\bar{A})$ is the empty set,
% it means that there is no set of states that for which there exists a word
% such that, when computed by $\bar{A}$,
% this word is accepted.
% With the set
% $X = \{ P,  \subseteq Q, P' \subseteq Q, P' \cap \bar{F} = \emptyset
% | \exists w \in \Sigma^* s.t. P \xrightarrow{w} P'\}$, it provides
% a way to check the existence such set of states.
% The goal is to show that if there exists
% no set of states $P'$ such that $P' \cap F = \emptyset$ i.e.
% $X = \emptyset$, then it is shown that $L(\bar{A}) = \emptyset$, therefore
% $A$ is universal. The difficulty of the problem arise at the computation
% of $X$. Checking intersection between the accepting states set and all
% the subsets is costly.
% \todo{Costly vs expensive vs complexe vs hard etc..}
% For a subset $P \subseteq Q$, for all subsets $p$ such that $p \subseteq P$,
% it is known that, computing the word $w$ starting from $p$ or $P$, will lead
% to the same final state. Therefore it is more interesting to compute
% the maximal sets $\ceil{X}$, which correspond to an antichain of set of
% states that are $\subseteq$-incomparable. In section \ref{data_structures},
% we give formal definition of notions such as maximal sets or incomparable
% elements.
% \todo{Include Xi explanation}


\paragraph{}

The algorithm proposed in \cite{antichain-universality}, doesn't build
the deterministic automaton, but rather does it implicitly.
It builds iteratively a set of subsets of states
that have as initial targets the non-accepting states.
At each iteration,
it adds direct
predecessors to the targets, until a fixed point is reached.
Also, at each iteration,
only maximal set of states, that is an antichain,
based on the inclusion partial order are kept
in memory for the next iteration.
The final condition is to check if the initial state is found
in the targets. It the initial state is included, it means that
there exists a word such that it is not accepted by
the automaton, it means that the automaton is not universal.


% In
% \cite{AC_universality}, the algorithm proposed follow an equivalent
% idea by using game theory. The universality problem is reduce to
% a two-player reachability game, which can be done in polynomial time.
% The objective of the game is for the protagonist to establish that
% the automaton $A$ is not universal. To this end, the protagonist will
% provide a word, a letter at a time, and find a strategy that try
% to show that $A$ ends in a rejecting state.
% The protagonist only has a strategy to win the game if and only if $A$ is
% not universal.
