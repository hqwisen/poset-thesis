% !TEX root = ../thesis.tex

\chapter{\texttt{poset}, an antichain library}

\label{chap:framework}

\section{Introduction}

The first contribution of this work is to provide a framework to interact with poset, antichains and partial orders. It comes as a \texttt{Java 11} library implementing the interfaces and classes for the data structures and operations. In this document we refer to those implementations as the \texttt{poset} library that is available in a git repository hosted on GitLab \cite{poset-src}.

\paragraph{}

In this chapter we go through the interfaces provided by the library to implement data structures and operations to interact with posets and antichains. We describe the Application Programming Interface (API) of the library and gives some example of utilisation. The examples are provided as \texttt{Java} snippets that can be executed directly in \texttt{jshell} \cite{jshell}, an interactive shell for \texttt{Java}, or used in other \texttt{Java} code. One can use the bash script \textbf{scripts/jshell}, that loads all the libraries including \texttt{Owl} and \texttt{poset}, available in the repository. Note that there are some prerequisites to execute the interactive shell and an installation guide is given in Appendix \ref{appendix:howto}.

For more details about the implementation go to chapter \ref{chap:impl}. For the complete \texttt{Javadoc}, follow the instructions given in Appendix \ref{appendix:howto} to generate it.

\paragraph{Main focus}

The main focus of the framework implementation is to provide a way to represent
posets using a common interface, and to provide an environment of development where developers can extend the library providing their own implementations depending on the universe that are required for their use cases. The implementation relies mostly on built-ins of the language (here we use \texttt{Java}) to make it as modular as possible.

\subsection{Default \& user implementation}

\label{sec:framework:default}

In the following sections we will give the description of all the interfaces with some examples. An interface is a list of methods and the examples use the default implementations coming with the \texttt{poset} library. We use two kind of posets in our examples. The first poset is $\langle S, \subseteq \rangle$ where $S$ is a set of generic object and $\subseteq$ is the inclusion partial order. The second poset is $\langle \mathbb{N}^2_{\leq 3}, \preceq \rangle$ where $\mathbb{N}^2_{\leq 3}$ the set of pairs of natural numbers in $[0, 3]$ and $\preceq$ is a partial order such that for all
$(n_1, n_1'), (n_2, n_2') \in \mathbb{N}^2$, then $(n_1, n_1') \preceq (n_2, n_2')$ if and only if $n_1 \leq n_2$ and $n_1' \leq n_2'$. We use the \texttt{Set} type objects to interact with the first poset, and \texttt{List} type objects for the second poset. Note that the \texttt{poset} library uses the \texttt{Java} generic types for all the interfaces, so any other base type can be used.

\paragraph{}

We refer to \textit{default implementation} to implementations of the interfaces that are used in the examples. Another developer that would like to change or create new implementations of the interfaces will extends them and create new classes with his custom implementations.
We refer to those as \textit{user implementation}. It means that they are not available in the \texttt{poset} library but implemented by developers to fit their uses cases.

The library can then be used to implement antichain algorithms. This is what will be performed in chapter \ref{chap:owl} were we implement the antichain algorithm to check the universality of finite automata and integrate it in \texttt{Owl} $\omega$-automata library.

\section{Order}


An order is a binary relation noted $s_1Rs_2$. In the library all orders are implemented as ascending, that
is the relation the default behavior is to return \texttt{true} if the first element is smaller (or equal) than
the second one. Therefore by default an order is symbolized using $\preceq$ (or $\prec$) and to obtain
a descending one, the an ascending order can be reversed using a function.

\paragraph{}

Following its definition, an order can be implemented as a binary function that returns \texttt{true} if
$s_1 \preceq s_2$, that is $s_1$ is smaller or equal to $s_2$, and return \texttt{false} otherwise.
Java provides functional interface to implement such operation that can be used with the functional programming
concept introduced in \texttt{Java 8}.

\subsection{Interface}

\subsection*{\texorpdfstring{\texttt{@FunctionalInterface\ \\ public\ interface\ Order\textless{}T\textgreater{}\ extends\ BiPredicate\textless{}T,\ T\textgreater{}}}{@FunctionalInterface public interface Order\textless T\textgreater{} extends BiPredicate\textless T, T\textgreater{}}}\label{functionalinterface-public-interface-ordert-extends-bipredicatet-t}

Order interface.

Boolean function that takes two parameters of the same type and return
\texttt{true} or \texttt{false}.

\subsection*{\texorpdfstring{\texttt{default\ boolean\ compare(T\ a,\ T\ b)\ throws\ Incomparable}}{default boolean compare(T a, T b) throws Incomparable}}\label{default-boolean-comparet-a-t-b-throws-incomparable}

Compare two elements together. A comparison is the result of the
computation of \(a R b\).

\begin{itemize}
\item
  \textbf{Parameters:}

  \begin{itemize}
  \item
    \texttt{a} --- First element to compare
  \item
    \texttt{b} --- Second element to compare
  \end{itemize}
\item
  \textbf{Returns:} true if a \texttt{R} b, false if b \texttt{R} a. If
  both are false, throws Incomparable.
\item
  \textbf{Exceptions:} \texttt{Incomparable} --- if both elements are
  incomparable.
\end{itemize}

\subsection*{\texorpdfstring{\texttt{boolean\ test(T\ t1,\ T\ t2)}}{boolean test(T t1, T t2)}}\label{boolean-testt-t1-t-t2}

Evaluates the order. If the elements are incomparable no exception will
be thrown, and \texttt{false} will be returned.

\begin{itemize}
\tightlist
\item
  \textbf{Parameters:}

  \begin{itemize}
  \item
    \texttt{t1} --- the first input argument
  \item
    \texttt{t2} --- the second input argument
  \end{itemize}
\item
  \textbf{Returns:} \texttt{true} if \(a R b\), false if \(b R a\) or
  if both are false.
\end{itemize}

\subsection*{\texorpdfstring{\texttt{@Override\ default\ Order\textless{}T\textgreater{}\ negate()}}{@Override default Order\textless T\textgreater{} negate()}}\label{override-default-ordert-negate}

Negate (reverse) an order.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} The reversed order.
\end{itemize}


\subsection{Default implementation}

Three orders are provided with the library.

\begin{itemize}
  \item The inclusion order $\subseteq$ that compare two sets $a, b$ and returns \texttt{true} if $a \subseteq b$. This example also demonstrates the behaviour of \texttt{negate()} method to compute $a \not\subseteq b$. The class that implements this order is \texttt{Include} and has the following signature:
  \begin{lstlisting}[numbers=none]
class Include<T> implements Order<Set<T>>
  \end{lstlisting}
  \begin{example}
    \label{negate-example}
    Snippet that test the inclusion $\{1\} \subseteq \{1, 2\}$, the incomparable elementes $\{1, 2\} \subseteq \{2, 3\}$ and the negation $\{1\} \not\subseteq \{1, 2\}$.
    \begin{lstlisting}[numbers=none]
import poset.orders.Include
var a = Set.of(1);
// a ==> [1]
var b = Set.of(1, 2);
// b ==> [2, 1]
var c = Set.of(2, 3);
// c ==> [3, 2]
var include = new Include()
include.compare(a, b)
// $ ==> true
var notInclude = include.negate()
notInclude.compare(a, b)
// $ ==> false
include.compare(b, c)
|  Exception poset.exceptions.Incomparable
|        at Order.compare (Order.java:40)
    \end{lstlisting}
  \end{example}
  \item The partial order $\sqsubseteq$ defined in section \ref{sec:prelim:orders}
  The class implementing this order is \texttt{SubsetEq} and has the following signature:
  \begin{lstlisting}[numbers=none]
class SubsetEq<T> implements Order<Set<Set<T>>>
  \end{lstlisting}
  \begin{example}
    Snippet that test if $\{\{0\}, \{1, 0\}, \{2, 0\}, \{2, 1\}\} \sqsubseteq \{\{2, 0, 1\}, \{0\}\}$.
    \begin{lstlisting}[numbers=none]
import poset.orders.SubsetEq
var subsetEq = new SubsetEq<Integer>();
Set<Set<Integer>> setOfSet0 = Set.of(
    Set.of(0), Set.of(0, 2), Set.of(0, 1), Set.of(1, 2)
);
setOfSet0 ==> [[0], [1, 0], [2, 0], [2, 1]]
Set<Set<Integer>> setOfSet1 = Set.of(
    Set.of(0), Set.of(0, 1, 2)
);
setOfSet1 ==> [[2, 0, 1], [0]]
subsetEq.compare(setOfSet0, setOfSet1)
// $ ==> true
subsetEq.compare(setOfSet1, setOfSet0)
// $ ==> false
    \end{lstlisting}
  \end{example}
  \item The partial order $\preceq$ as defined in section \ref{sec:framework:default}. The class implementing this order is \texttt{LEQ} (lower or equal) and the signature is:
  \begin{lstlisting}[numbers=none]
class LEQ implements Order<List<Integer>>
  \end{lstlisting}
  \begin{example}
    Snippet comparing the elements $(3, 4)$, $(4, 3)$ and $(1, 2)$ using $\preceq$ order.
    \begin{lstlisting}[numbers=none]
var order = new LEQ();
var v34 = List.of(3, 4);
var v43 = List.of(4, 3);
var v12 = List.of(1, 2);
leq.compare(v12, v34);
// $ ==> true
leq.compare(v12, v43);
// $ ==> true
leq.compare(v34, v12);
// $ ==> false
leq.compare(v43, v12);
// $ ==> false
    \end{lstlisting}
  \end{example}
\end{itemize}

\section{Bound}

\subsection{Interface}

\subsection*{\texorpdfstring{\texttt{@FunctionalInterface\ \\ public\ interface\ Bound\textless{}T\textgreater{}}}{@FunctionalInterface public interface Bound\textless T\textgreater{}}}\label{functionalinterface-public-interface-boundt}

Implements the lower bound of \(P \subseteq S\) (for a poset
\(\langle S, \preceq \rangle\)).

\subsection*{\texorpdfstring{\texttt{T\ compute(T\ a,\ T\ b,\ Order\textless{}T\textgreater{}\ order)}}{T compute(T a, T b, Order\textless T\textgreater{} order)}}\label{t-computet-a-t-b-ordert-order}

Computation of the bound operation \(a \sqcap b\)

\begin{itemize}
\tightlist
\item
  \textbf{Parameters:}

  \begin{itemize}
  \tightlist
  \item
    \texttt{a} --- First element of the bound operation
  \item
    \texttt{b} --- Second element of the bound operation
  \item
    \texttt{order} --- order of the poset
  \end{itemize}
\item
  \textbf{Returns:} the lower bound of \(P\)
\end{itemize}

\subsection{Default implementation}

One default implementation for the bounds is provided. The \texttt{GreatestLowerBound} bound function compute the greatest lower bound of two integer vectors from the poset $\langle S, \preceq \rangle$. The \texttt{GreatestLowerBound} class has the following signature:

\begin{lstlisting}[numbers=none]
class GreatestLowerBound implements Bound<List<Integer>>
\end{lstlisting}

\begin{example}
  This snippet computes the greatest lower bound of the two elements $(7, 2) \sqcap (6, 7)$ for the poset $\langle S, \preceq \rangle$ introduced in section \ref{sec:framework:default}.
  \begin{lstlisting}[numbers=none]
import poset.orders.LEQ;
import poset.bounds.GreatestLowerBound;
List<Integer> v72 = List.of7, 2);
// v72 ==> [7, 2]
List<Integer> v67 = List.of(6, 7);
// v67 ==> [6, 7]
var leq = new LEQ();
var glb = new GreatestLowerBound();
glb.compute(v67, v72, leq);
// $   ==> [6, 2]
  \end{lstlisting}

\end{example}

\section{Antichain}

\label{sec:framework:antichain}

An antichain is a set of incomparable elements. It is implemented as an extension of the built-in Java \texttt{Set}
interfaces, in the \texttt{Antichain} interface. The default implementation provided for this interface is \texttt{AntichainList} which implements the \texttt{Antichain} interface using a \texttt{LinkedList} to store the incomparable elements.

\subsection{Interface}

\subsection*{\texorpdfstring{\texttt{public\ interface\ Antichain\textless{}E\textgreater{}\ extends\ Set\textless{}E\textgreater{}}}{@SuppressWarnings("override") public interface Antichain\textless E\textgreater{} extends Set\textless E\textgreater{}}}\label{suppresswarningsoverride-public-interface-antichaine-extends-sete}

Antichain interface to implement a set of incomparable elements.

Antichain \(\alpha\) of a poset \(\langle S, \preceq \rangle\) where
\(\alpha \subseteq S\). The implementations of this class should use an
order (implementing the \(\preceq\) partial order of the poset) and
store a collection of incomparable elements.

\begin{itemize}
\tightlist
\item
  \textbf{Parameters:} \texttt{\textless{}E\textgreater{}} --- Type of
  the elements from the set \(S\).
\end{itemize}

\subsection*{\texorpdfstring{\texttt{int\ size()}}{int size()}}\label{int-size}

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} the number of incomparable elements.
\end{itemize}

\subsection*{\texorpdfstring{\texttt{boolean\ isEmpty()}}{boolean isEmpty()}}\label{boolean-isempty}

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} true if the antichain contains no elements, false
  otherwise.
\end{itemize}

\subsection*{\texorpdfstring{\texttt{boolean\ contains(Object\ o)}}{boolean contains(Object o)}}\label{boolean-containsobject-o}

Return true if the element is on of the incomparable elements of the
antichain.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{o} --- element whose presence in the
  antichain is to be tested
\item
  \textbf{Returns:} \texttt{true} if antichain contains the element
\end{itemize}


\subsection*{\texorpdfstring{\texttt{boolean\ add(E\ e)}}{boolean add(E e)}}

Add an element to the antichain.

If the element is smaller of any element of the antichain, no operation
will be performed. If the element is bigger than any of the element of
the antichain, it is added to the sequence of incomparable elements, and
every elements that are smaller to the new one will be removed.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{e} --- element to add.
\item
  \textbf{Returns:} true if the element was added to the incomparable
  elements, false otherwise.
\end{itemize}

\begin{example}
  Example of addition in an antichain.
  \begin{lstlisting}[numbers=none,language=java]
Antichain<Set<Integer>> a = new AntichainList<>(Include::order);
a.add(Set.of(1, 2)); a.add(Set.of(2, 1));
// a ==> [[1, 2]]
a.add(Set.of(3, 1, 2));
// a ==> [[3, 1, 2]]
  \end{lstlisting}
\end{example}

\subsection*{\texorpdfstring{\texttt{boolean\ remove(Object\ o)}}{boolean remove(Object o)}}\label{boolean-removeobject-o}

Remove an element from the incomparable elements.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{o} --- element to remave
\item
  \textbf{Returns:} true if the element was removed, false otherwise.
\end{itemize}

\subsection*{\texorpdfstring{\texttt{Antichain\textless{}E\textgreater{}\ union(Antichain\textless{}E\textgreater{}\ other)}}{Antichain\textless E\textgreater{} union(Antichain\textless E\textgreater{} other)}}\label{antichaine-unionantichaine-other}

Perform an union operation between two antichains. The results is a new
antichain that contains the incomparable elements of both antichains.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} New antichain representing the union of both
  antichains.
\end{itemize}

\begin{example}
  \label{ex:framework:antichain:union}
  Example of antichains union using the poset $\langle \mathbb{N}^2_{\leq 3}, \preceq \rangle$.
  \begin{lstlisting}[numbers=none]
import poset.orders.LEQ
import poset.bounds.GreatestLowerBound
import poset.AntichainList
var leq = new LEQ() // The partial order
var glb = new GreatestLowerBound()
var a1 = new AntichainList<>(leq, glb)
// a1 ==> []
a1.add(List.of(3, 1))
//  a1 ==> [[3, 1]]
a1.add(List.of(2, 2))
// a1 ==> [[3, 1], [2, 2]]
var a2 = new AntichainList<>(leq, glb)
// a2 ==> []
a2.add(List.of(0, 2))
// a2 ==> [[0, 2]]
a1.union(a2)
// $ ==> [[3, 1], [2, 2]]
 a2.add(List.of(2, 3))
// a2 ==> [[2, 3]]
a1.union(a2)
// $ ==> [[3, 1], [2, 3]]
  \end{lstlisting}
\end{example}

\subsection*{\texorpdfstring{\texttt{Antichain\textless{}E\textgreater{}\ intersection(Antichain\textless{}E\textgreater{}\ other)}}{Antichain\textless E\textgreater{} intersection(Antichain\textless E\textgreater{} other)}}\label{antichaine-intersectionantichaine-other}

Perform an intersection between two antichains.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} a new antichain, that is the intersection.
\end{itemize}

\begin{example}
  Reusing the same instanciation maed in Example \ref{ex:framework:antichain:union} above, the following snippet performs the intersection of the antichains $\alpha_1 = \{(3, 1), (2, 2)\}$ and
  $\{(2, 3)\}$ of the poset $\langle \mathbb{N}^2_{\leq 3}, \preceq \rangle$, i.e. $\alpha_1 \cap \alpha_2 = \{(2, 2)\}$.
  \begin{lstlisting}[numbers=none]
// a1 ==> [[3, 1], [2, 2]]
// a2 ==> [[2, 3]]
a1.intersection(a2)
// $  ==> [[2, 2]]
  \end{lstlisting}
\end{example}

\subsection*{\texorpdfstring{\texttt{Order\textless{}E\textgreater{}\ order()}}{Order\textless E\textgreater{} order()}}\label{ordere-order}

Order \(\preceq\) of the poset.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} the order
\end{itemize}

\section{Closed set}

A closed set is a subset of $S$ and closed for a partial order $\prec$ of a lower semilattice
$\langle S, \preceq \rangle$. The \texttt{poset.ClosedSet} interface is provided
to interact with a closed set,
including a default implementation that stores the maximal element as an \texttt{AntichainList}.

\subsection{Interface}

\subsection*{\texorpdfstring{\texttt{public\ interface\ ClosedSet\textless{}E\textgreater{}}}{public interface ClosedSet\textless E\textgreater{}}}\label{public-interface-closedsete}

Interface for closed set representation.

A closed set is a subset of \(S\) and closed for a partial order
\(\preceq\) of a lower semilattice \(\langle S, \preceq \rangle\). This
interface provides the basic methods that must be supported for
interacting with closed sets.

\begin{itemize}
\tightlist
\item
  \textbf{Parameters:} \texttt{\textless{}E\textgreater{}} --- type of
  element \(s\) of the closed set, with \(s \in S\).
\end{itemize}

\subsection*{\texorpdfstring{\texttt{boolean\ add(E\ e)}}{boolean add(E e)}}

Add an element to the closed set.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{e} --- element to add in the closed set.
\item
  \textbf{Returns:} \texttt{true} if the closed set internal
  representation has changed
\end{itemize}

\hypertarget{boolean-addallcollection-extends-e-c}{%
\subsection*{\texorpdfstring{\texttt{boolean\ addAll(Collection\textless{}?\ extends\ E\textgreater{}\ c)}}{boolean addAll(Collection\textless? extends E\textgreater{} c)}}\label{boolean-addallcollection-extends-e-c}}

Add all of the elements in the specified collection.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{c} --- collection containing the elements
  to be added to this closed set
\item
  \textbf{Returns:} \texttt{true} if the closed set internal
  representation has changed
\end{itemize}

\hypertarget{boolean-iscanonical}{%
\subsection*{\texorpdfstring{\texttt{boolean\ isCanonical()}}{boolean isCanonical()}}\label{boolean-iscanonical}}

Each implementation should specify whether it uses the canonical
representation of a closed set.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} \texttt{true} if the elements are stored using a
  canonical representation
\end{itemize}

\hypertarget{boolean-containse-e}{%
\subsection*{\texorpdfstring{\texttt{boolean\ contains(E\ e)}}{boolean contains(E e)}}\label{boolean-containse-e}}

Return true if the element is in the lower closure of the closed set.

\begin{itemize}
\item
  \textbf{Parameters:} \texttt{e} --- element whose presence in the
  closed set is to be tested
\item
  \textbf{Returns:} \texttt{true} if closed set contains the element
\end{itemize}

\hypertarget{closedsete-computeboundclosedsete-cs}{%
\subsection*{\texorpdfstring{\texttt{ClosedSet\textless{}E\textgreater{}\ computeBound(ClosedSet\textless{}E\textgreater{}\ cs)}}{ClosedSet\textless E\textgreater{} computeBound(ClosedSet\textless E\textgreater{} cs)}}\label{closedsete-computeboundclosedsete-cs}}

Compute the bound between two closed sets. The computation consists into
building a new closed set representation of the bound values of all
elements of both closed sets.

\hypertarget{closedsete-intersectionclosedsete-other}{%
\subsection*{\texorpdfstring{\texttt{ClosedSet\textless{}E\textgreater{}\ intersection(ClosedSet\textless{}E\textgreater{}\ other)}}{ClosedSet\textless E\textgreater{} intersection(ClosedSet\textless E\textgreater{} other)}}\label{closedsete-intersectionclosedsete-other}}

Compute the intersection of two closed sets. The intersection consists
of the closure of the bound computation.

\hypertarget{collectione-elements}{%
\subsection*{\texorpdfstring{\texttt{Collection\textless{}E\textgreater{}\ elements()}}{Collection\textless E\textgreater{} elements()}}\label{collectione-elements}}

Return the elements of the closed set.

If the closed set uses a cononical representation, this method should
only return the element of such representation. If all the elements are
required, use \texttt{ClosedSet.closureElements()}
method.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} collection of elements composing the closed set
\end{itemize}

\hypertarget{collectione-closureelements}{%
\subsection*{\texorpdfstring{\texttt{Collection\textless{}E\textgreater{}\ closureElements()}}{Collection\textless E\textgreater{} closureElements()}}\label{collectione-closureelements}}

Return all the elements contained in the closed set.

Return the lower closure \(\darrow \ceil{L}\) of the closed set,
i.e.~all the elements of the closed set.

\begin{itemize}
\tightlist
\item
  \textbf{Returns:} all the elements of the closed set
\end{itemize}
